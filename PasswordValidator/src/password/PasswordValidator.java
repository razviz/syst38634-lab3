package password;
/*
 * 
 * @author Zain Razvi - 991517290
 * 
 * Validates passwords and is being developed using TDD
 * 
 */
public class PasswordValidator {
	
	private static int MIN_LENGTH = 8;
	
	/**
	 * 
	 * @param password
	 * @return true if the number of characters is 8 or more. No spaces are allowed.
	 * Opted for a single return rather than 2 separate returns.
	 */
	public static boolean isValidLength(String password) {		
	
		//Refactor for Exception
		return password.trim().length() >= MIN_LENGTH;
		
	}
	/**
	 * 
	 * @param pass
	 * @return true if the password has 2 digits or more.
	 * Opted for one if statement instead of 5
	 */
	public static boolean hasTwoDigit(String pass) {
		int count = 0;
		char[] password = pass.toCharArray();
	
		for(char a : password) {
			if(Character.isDigit(a)) {
				count++;
			}
		}
		return count >= 2;
	}
	
	public static boolean hasValidCaseChars(String password) {
		//return password != null && password.matches(".*([^A-Z]*[A-Z]){2}.*");
		return password != null && password.matches("^.*[a-z].*$") && password.matches("^.*[A-Z].*$");
	}
	
	
}
