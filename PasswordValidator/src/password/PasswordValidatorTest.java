package password;

import static org.junit.Assert.*;

import org.junit.Test;

/*
 * 
 * @author Zain Razvi - 991517290
 * 
 */

public class PasswordValidatorTest {
	
	@Test
	public void testHasValidCaseCharsRegular() {
		assertTrue("Invalid case chars", PasswordValidator.hasValidCaseChars("aBAaksiB"));
	}
		
	@Test
	public void testHasValidCaseCharsBoundaryIn() {
		assertTrue("Invalid case chars", PasswordValidator.hasValidCaseChars("hT"));
	}
	
	@Test
	public void testHasValidCaseCharsException() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("783737"));
	}
	
	@Test
	public void testHasValidCaseCharsExceptionBlank() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars(""));
	}
	
	@Test
	public void testHasValidCaseCharsExceptionNull() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars(null));
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryOutUpper() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("BKKIYF"));
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryOutLower() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("aaaa"));
	}
	
	

	@Test
	public void TestIsValidLengthRegular() {
		//to pass
		boolean result = PasswordValidator.isValidLength("1234567890");
		assertTrue("Invalid length", result);
	}
	
	@Test
	public void testIsValidLengthException() {	
		//to pass
		boolean result = PasswordValidator.isValidLength("");
		assertFalse("Invalid length", result);
	}
	
	@Test
	public void testIsValidLengthBoundaryIn() {
		//to pass
		boolean result = PasswordValidator.isValidLength("12345678");
		assertTrue("Invalid length", result);
	}
	
	@Test
	public void testIsValidLengthBoundaryOut() {	
		//to pass
		boolean result = PasswordValidator.isValidLength("1234567");
		assertFalse("Invalid length", result);
	}

	
	
	@Test
	public void TestHasTwoDigitRegular() {	
		//to pass
		boolean result = PasswordValidator.hasTwoDigit("zainer1234");
		assertTrue("Password does not contain 2 digits", result);
	}
	
	@Test
	public void TestHasTwoDigitException() {
		//to pass
		boolean result = PasswordValidator.hasTwoDigit("zainer");
		assertFalse("Password does not contain 2 digits", result);
	}
	
	@Test
	public void TestHasTwoDigitBoundaryIn() {
		//to pass
		boolean result = PasswordValidator.hasTwoDigit("zainer12");
		assertTrue("Password does not contain 2 digits", result);
	}
	
	@Test
	public void TestHasTwoDigitBoundaryOut() {	
		//to pass
		boolean result = PasswordValidator.hasTwoDigit("zainer1");
		assertFalse("Password does not contain 2 digits", result);
	}
	
	
	
	
	
	/*@Test
	public void testIsValidLengthExceptionSpaces() {
		//to fail
		//fail("Invalid length");
		boolean result = PasswordValidator.isValidLength("    test      ");
		assertFalse("Invalid length", result);
	}*/
	
	
}
